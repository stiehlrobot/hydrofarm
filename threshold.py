class Threshold:

    def __init__(self, conn):

        self.conn = conn

    def get_id(self, thresholdName):

        cursorObj = self.conn.cursor()
        cursorObj.execute("SELECT id FROM thresholds WHERE name="+thresholdName+"")
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result
    
    def update_value(self, newValue, thresholdId):

        self.updateSQL("value", newValue, thresholdId)

    def get_value(self, thresholdId):

        return self.getSQL("value", thresholdId)        

    def update_name(self, newName, thresholdId):

        self.updateSQL("name", newName, thresholdId)

    def get_name(self, thresholdId):

        return self.getSQL("name", thresholdId)
        
    def update_unit(self, newUnitName, thresholdId):

        self.updateSQL("measurement_unit", newUnitName, thresholdId)

    def get_unit(self, thresholdId):

        return self.getSQL("measurement_unit", thresholdId)

    def update_hydro_id(self, newHydroId, thresholdId):

        self.updateSQL("hydromodule_id", newHydroId, thresholdId)
        
    def get_hydromodule_id(self, thresholdId):

        return self.getSQL("hydromodule_id", thresholdId)   


    """
    ***************************************************
    SQL UTILITY FUNCTIONS
    ***************************************************
    """

    def getSQL(self, itemToFetch, thresholdId):

        result=None
        cursorObj = self.conn.cursor()
        query = "SELECT "+itemToFetch+" FROM thresholds WHERE id="+thresholdId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def updateSQL(self, itemToUpdate, newValue, thresholdId):

        
        cursorObj = self.conn.cursor()
        query = "UPDATE thresholds SET "+itemToUpdate+"="+newValue+" WHERE id ="+thresholdId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
    
        except Error:

            print(Error)

        finally:
            self.conn.close()  