# Hydrofarm - an automated hydroponics / Kratky vertical farm installation 

## Purpose 

For learning about designing, building and managing an automated farm system remotely.

## Documentation

[wiki](https://gitlab.com/stiehlrobot/hydrofarm/-/wikis/home)

## Project status

Phase 1 - Proof of concept (MVP), **COMPLETE**

Phase 2 - Sensor modules and data logging, **WIP** estimated completion > Q3, 2021

Phase 3 - Flow control, **2022** 
