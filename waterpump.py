"""
Module to control a 12V water pump using a logic level mosfet like IRF520 
"""


import time
try:
    import RPi.GPIO as GPIO
    test_environment = False
except(ImportError, RuntimeError):
    test_environment = True

class Waterpump:

    def __init__(self, name, pumpGateControlPin):
        self.startTime = time.time()
        self.name = name
        self.pump_ON = False
        self.timerInterval = None
        self.timeOn = None
        self.pumpControlPin = pumpGateControlPin
        if not test_environment:
            # to use Raspberry Pi board pin numbers
            GPIO.setmode(GPIO.BOARD)
            # set up the GPIO channels - one input and one output
            GPIO.setup(self.pumpControlPin, GPIO.OUT)
            GPIO.output(self.pumpControlPin, GPIO.HIGH)

    def pump_off(self):
        if not test_environment:
            GPIO.output(self.pumpControlPin, GPIO.LOW)        
        print("Pump off")
        self.pump_ON = False

    def pump_on(self):

        if not test_environment:
            GPIO.output(self.pumpControlPin, GPIO.HIGH)
        print("Pump on")
        self.pump_ON = True

    def get_pump_state():

        return self.pump_ON

    def set_pump_timer_interval(self, timeInterval, timeOn):

        self.timerInterval = timeInterval
        self.timeOn = timeOn
        print("Set pump to be on for "+timeOn+" between "+timeInterval+" intervals.")

    def start_timed_pumping(self):
        startTime = time.time()
        if self.timerInterval is not None and self.timeOn is not None:
            while true:
                if time.time() < startTime + self.timerInterval:
                    time.sleep(1)
                else:
                    self.pump_on()
                    print("Hit timer interval: pump ON")
                    time.sleep(self.timeOn)                    
                    self.pump_off()
                    print("Hit time on threshold: pump OFF")
                    startTime = time.time()
    
      

