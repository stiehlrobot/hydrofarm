import threshold as T
import plants as P
import sensor as S
import hydromodule as H
import waterpump as WP
import hydromodule_log as HLog
import time
import sqlite3
from sqlite3 import Error
import re

conn = None
h = None
s = None
p = None
t = None
hydro_log = None

def connect_to_db():

    global conn 
    try:

        conn = sqlite3.connect('sqldb/hydrofarm.db')
        print("Database connection established")

    except Error:

        print(Error)

def close_db_connection():
    global conn
    try:

        conn.close()

    except Error:

        print(Error)

def init():

    global conn, h , s , t , p , hydro_log
    
    connect_to_db()
    
    h = H.HydroModule(conn)
    s = S.Sensor(conn)
    t = T.Threshold(conn)
    p = P.Plants(conn)

    
    

def test_timed_pumping(interval, pumpOnTime):
    #create a pump and set name and pumpcontrol GPIO pin
    pump = WP.Waterpump("his majesty", 21) 
    interval_seconds = interval
    startTime = time.time()
    while True:
        if (startTime + interval_seconds) < time.time():
            pump.pump_on()
            time.sleep(pumpOnTime)
            pump.pump_off()
            startTime = time.time()
        time.sleep(1)
    
if __name__=='__main__':

    init()
    #test_timed_pumping(10, 5)
    hydro_log = HLog.HydroLog(conn, s, h, t, p)
    hydromodule_ids = h.get_all_hydromodule_ids()

    #iterate over hydromodule ids and execute hydro_log.insertSQL_log_data() with all of the hydromodule ids
    print("Printing hydromodule ids: ")
    print(hydromodule_ids)
    for id in hydromodule_ids:        
        print(id)
        hydro_log.insertSQL_log_data(str(id))

    close_db_connection()


