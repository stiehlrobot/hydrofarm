import pygame
import pygame.camera
import pygame.capture_image
import sys
from datetime import datetime
from pygame.locals import *

IMG_SAVE_PATH = "/images/"

#camera module based on pygame https://www.pygame.org/docs/tut/CameraIntro.html
class Camera(object):

    global IMG_SAVE_PATH

    def __init__(self, deviceAddress):
        self.devAddress = deviceAddress
        
    def initialize(self):
        try:
            pygame.init()
            pygame.camera.init()
        except:
            print("error initializing camera")
    
    def capture_image(self, widthPixel, heightPixel, moduleName):
        '''capture image and save it with module id and timestamp'''
        try:
            cam = pygame.camera.Camera(self.devAddress,(widthPixel,heightPixel))
            cam.start()
            image = cam.get_image()
            pygame.image.save(image,IMG_SAVE_PATH+moduleName+"-"+get_timestamp()+".jpg")
        except:
            print("problem capturing and saving image")

    def get_timestamp():
        '''Return a timestamp'''
        now = datetime.now() # current date and time
        date_time = now.strftime("%d/%m/%Y-%H:%M:%S")
