class HydroModule:

    def __init__(self, conn):
        self.conn = conn

        
    def get_id(self, hydromoduleName):

        cursorObj = self.conn.cursor()
        cursorObj.execute("SELECT id FROM hydromodules WHERE name="+hydromoduleName+"")
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def get_all_hydromodule_ids(self):

        result = None
        self.conn.row_factory = lambda cursor, row: row[0]
        cursorObj = self.conn.cursor()
        query = "SELECT id FROM hydromodules"
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def update_type(self, newGrowthModType, hydromoduleId):

        self.updateSQL("type", newGrowthModType, hydromoduleId)   

    def get_type(self, hydromoduleId):

        return self.getSQL("type",hydromoduleId)

    def update_name(self, newName, hydromoduleId):

        self.updateSQL("name", newName, hydromoduleId)

    def get_name(self, hydromoduleId):

        return self.getSQL("name",hydromoduleId)

    def update_plantslots(self, newPlantSlotsAmount, hydromoduleId):

        self.updateSQL("plantSlots", newPlantSlotsAmount, hydromoduleId)        

    def get_plantslots(self, hydromoduleId):

        return self.getSQL("plantSlots",hydromoduleId)

    

    """
    ***************************************************
    SQL UTILITY FUNCTIONS
    ***************************************************
    """

    def getSQL(self, itemToFetch, hydromoduleId):

        result = None
        cursorObj = self.conn.cursor()
        query = "SELECT "+itemToFetch+" FROM hydromodules WHERE id="+hydromoduleId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def updateSQL(self, itemToUpdate, newValue, hydromoduleId):

        cursorObj = self.conn.cursor()
        query = "UPDATE hydromodules SET "+itemToUpdate+"="+newValue+" WHERE id ="+hydromoduleId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
    
        except Error:

            print(Error)