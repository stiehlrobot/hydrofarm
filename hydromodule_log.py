from datetime import datetime
import pytz

class HydroLog:

    def __init__(self, conn, sensors, hydromodules, thresholds, plants):

        self.conn = conn
        self.sensors = sensors
        self.hydromodules = hydromodules
        self.thresholds = thresholds
        self.plants = plants
    
    def insertSQL_log_data(self, hydromoduleId):

        '''gathers all data from other classes and inserts compilation log into database'''

        #name of hydromodule
        name = self.hydromodules.get_name(hydromoduleId)

        #type of hydromodule
        ttype = self.hydromodules.get_type(hydromoduleId)

        #amount of plants in hydromodule
        plant_amount = self.plants.get_total_of_plants_w_hydromodule_id(hydromoduleId)

        #amount of plantslots
        max_slots = self.hydromodules.get_plantslots(hydromoduleId)

        #plants and their amounts in hydromodule form dict to string
        plant_record = "plantname, amount"+"\n"
        plants_and_amounts = self.plants.get_plants_and_amounts(hydromoduleId)
        for plantName, amount in plants_and_amounts.items():
            plant_record += str(plantName)+", "+str(amount[0])+"\n"

        #image
        image = None

        #sensors and values
        sensors_and_values = None

        #current time
        country = 'Europe/Helsinki'
        tz_to_display = pytz.timezone(country)
        local_time_now = datetime.now()

        #hydroId
        hydromodule_id = hydromoduleId

        cursorObj = self.conn.cursor()

        print("Inserting into log: "+"\n")
        print(name)
        print(ttype)
        print(plant_amount)
        print(max_slots)
        print(image)
        print(plant_record)
        print(sensors_and_values)
        print(local_time_now)
        print(hydromodule_id)

        query = ("INSERT INTO hydromodule_log (name, type, current_capacity, plant_capacity, " 
        " time_now, hydromodule_id) values ({},{},{},{},{},{},{}"
        ")".format(name, ttype, plant_amount, max_slots, str(local_time_now), hydromodule_id))

        cursorObj.execute(query)
        try:

            self.conn.commit()

        except Error:

            print(Error)

    def updateSQL_log_w_id(self, itemToUpdate, newValue, logId):

        self.updateSQL(itemToUpdate, newValue, logId)

    def updateSQL_log_W_timestamp(self, itemToUpdate, newValue, timeStamp):

        '''Update a single value in a hydromodule_log entry with matching timestamp '''

        cursorObj = self.conn.cursor()
        query = "UPDATE hydromodule_log SET "+itemToUpdate+"="+newValue+" WHERE time_now ="+timeStamp+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
    
        except Error:

            print(Error)
        
        finally:
            self.conn.close()

    def getSQL_log_w_id(self, logId):
        '''return all logs with id'''

        return self.getSQL("*", logId)

    def getSQL_log_w_timestamp(self, timeStamp):
        ''''''
        return self.getSQL_w_timestamp("*", timeStamp)

    def updateSQL(self, itemToUpdate, newValue, logId):

        '''Updates a value in hydromodule_log table with a new value for a specific log'''

        cursorObj = self.conn.cursor()
        query = "UPDATE hydromodule_log SET "+itemToUpdate+"="+newValue+" WHERE id ="+logId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
    
        except Error:

            print(Error)
        
        finally:
            self.conn.close()

    def getSQL(self, itemToFetch, logId):

        '''Fetches a hydromodule_log with a specific timestamp'''

        result = None
        cursorObj = self.conn.cursor()
        query = "SELECT "+itemToFetch+" FROM hydromodule_log WHERE id="+logId
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def getSQL_w_timestamp(self, itemToFetch, timeStamp):

        '''Returns a hydromodule_log with a specific timestamp'''

        result = None
        cursorObj = self.conn.cursor()
        query = "SELECT "+itemToFetch+" FROM hydromodule_log WHERE time_now="+timeStamp
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result