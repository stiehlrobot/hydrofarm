#this file to act as sensor interface with classes and raspi sensor reading functionality

class Sensor(object):


    def __init__(self, conn):

        self.conn = conn
    
    def get_id(self, sensorName):

        cursorObj = self.conn.cursor()
        cursorObj.execute("SELECT id FROM sensors WHERE sensor_name="+sensorName+"")
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

    def get_value(self, sensorId):
        
        return self.getSQL("value", sensorId)
    
    def update_value(self, newValue, sensorId):

        self.updateSQL("value", newValue, sensorId)

    def get_sensor_name(self, sensorId):

        return self.getSQL("sensor_name", sensorId)

    def update_sensor_name(self, newSensorName, sensorId):

        self.updateSQL("sensor_name", newSensorName, sensorId)

    def update_measurement_unit(self, newMeasurementUnit, sensorId):

        self.updateSQL("measurement_unit", newMeasurementUnit, sensorId)

    def get_measurement_unit(self, sensorId):

        return self.getSQL("measurement_unit", sensorId)

    def get_hydromodule_id(self, sensorId):

        return self.getSQL("hydromodule_id", sensorId)

    def update_hydromodule_id(self, hydroId, sensorId):

        self.updateSQL("hydromodule_id", newValue, sensorId)


    """
    ***************************************************
    SQL UTILITY FUNCTIONS
    ***************************************************
    """

    def getSQL(self, itemToFetch, sensorId):

        result = None
        cursorObj = self.conn.cursor()
        query = "SELECT "+itemToFetch+" FROM sensors WHERE id="+sensorId
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def updateSQL(self, itemToUpdate, newValue, sensorId):

        cursorObj = self.conn.cursor()
        query = "UPDATE sensors SET "+itemToUpdate+"="+newValue+" WHERE id ="+sensorId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
    
        except Error:

            print(Error)
        
        finally:
            self.conn.close()  