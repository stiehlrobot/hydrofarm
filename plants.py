class Plants:

    def __init__(self, conn):

        self.conn = conn   

    def get_id(self, plantName):

        cursorObj = self.conn.cursor()
        cursorObj.execute("SELECT id FROM plants WHERE name="+plantName+"")
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def update_plant_name(self, newPlantName, plantId):

        self.updateSQL("plantname", newPlantName, plantId)

    def get_plant_name(self, plantId):

        return self.getSQL("plantname", plantId)

    def get_all_plant_names_in_hydromodule(self, hydromoduleId):
        plantList = list()
        query = "SELECT DISTINCT plantname FROM plants WHERE id="+hydromoduleId
        result = self.executeSQL_with_return_values(query)
        for row in result:
            plantList.append(row)
        return plantList

    def update_plants_amount(self, newPlantsAmount, plantId):

        self.updateSQL("amount", newPlantsAmount, plantId)

    def get_plants_amount(self, plantId):

        return self.getSQL("amount", plantId)
    
    def get_total_of_plants_w_hydromodule_id(self, hydromoduleId):

        query = "SELECT SUM(amount) FROM plants WHERE id="+hydromoduleId
        return self.executeSQL_with_return_values(query)

    def get_plants_and_amounts(self, hydromodule_id):
        plants_and_amounts = dict()
        plantNames = self.get_all_plant_names_in_hydromodule(hydromodule_id)
        for plant in plantNames:
            print(plant, hydromodule_id)
            plants_and_amounts[plant] = self.get_amount_of_plant_type_w_hydromodule_id(plant, hydromodule_id)
        return plants_and_amounts  

    def get_amount_of_plant_type_w_hydromodule_id(self, plantName, hydromoduleId):

        query = "SELECT SUM(amount) FROM plants WHERE hydromodule_id="+hydromoduleId+" AND plantname='"+plantName+"'"
        return self.executeSQL_with_return_values(query)

    def update_germination_time_days(self, newGerminationTimeDays, plantId):

        self.updateSQL("germination_time_days", newGerminationTimeDays, plantId)

    def get_germination_time_days(self, plantId):
    
        return self.getSQL("germination_time_days", plantId)

    def update_germination_temp_celsius(self, newGerminationTempCelsius, plantId):

        self.updateSQL("germination_temperature_celsius", newGerminationTempCelsius, plantId)

    def get_germination_temp_celsius(self, plantId):

        return self.getSQL("germination_temperature_celsius", plantId)

    def update_ph_start(self, newStartPh, plantId):

        self.updateSQL("ph_start", newStartPh, plantId)

    def get_ph_start(self, plantId):

        return self.getSQL("ph_start", plantId)
    
    def update_start_time(self, newStartTime, plantId):

        self.updateSQL("start_time", newStartTime, plantId)

    def get_start_time(self, plantId):

        return self.getSQL("ph_minimun", plantId)
    
    def update_ph_min(self, newMinimumPh, plantId):

        self.updateSQL("ph_minimum", newMinimumPh, plantId)

    def get_ph_min(self, newPhMin):

        return self.getSQL("ph_minimun", plantId)

    def update_ph_max(self, newMaximumPh, plantId):

        self.updateSQL("ph_maximum", newMaximumPh, plantId)

    def get_ph_max(self, plantId):

        return self.getSQL("ph_maximum", plantId)
    
    def update_hydromodule_id(self, newHydroId, plantId):

        self.updateSQL("hydromodule_id", newHydroId, plantId)

    def get_hydromodule_id(self, plantId):

        return self.getSQL("hydromodule_id", plantId)


    """
    ***************************************************
    SQL UTILITY FUNCTIONS
    ***************************************************
    """

    def getSQL(self, itemToFetch, hydromoduleId):

        result = None
        cursorObj = self.conn.cursor()
        query = "SELECT "+itemToFetch+" FROM plants WHERE id="+plantId
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result

    def updateSQL(self, itemToUpdate, newValue, hydromoduleId):

        cursorObj = self.conn.cursor()
        query = "UPDATE plants SET "+itemToUpdate+"="+newValue+" WHERE id ="+plantId+""
        cursorObj.execute(query)
        try:

            self.conn.commit()
    
        except Error:

            print(Error)

        finally:
            self.conn.close()

    def executeSQL_with_return_values(self, query):

        result = None
        cursorObj = self.conn.cursor()
        cursorObj.execute(query)
        try:

            self.conn.commit()
            result = cursorObj.fetchall()

        except Error:

            print(Error)

        return result